// w tym pliku bedą ostylowane komponenty dla nawigacji

import { NavLink } from 'react-router-dom';

import styled from 'styled-components';

export const StyledNavLink = styled(NavLink)`
  padding: 10px 20px;
  background-color: lightblue;
  display: block;
  border: 1px solid black;
  color: black;
`;
