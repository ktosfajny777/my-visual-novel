import React from 'react';
import './App.css';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { StyledNavLink } from 'components/Nav/nav.styled';

function App() {
  return (
    <>
      <Router basename={process.env.PUBLIC_URL}>
        <nav
          style={{ height: '100px', backgroundColor: 'red', display: 'flex' }}
        >
          <StyledNavLink to="/">HOME</StyledNavLink>
          <StyledNavLink to="/budget">BUDGET</StyledNavLink>
        </nav>

        <Switch>
          <Route exact path="/">
            homeasdaspage
          </Route>
          <Route exact path="/budget">
            sadasd
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
